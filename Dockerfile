FROM alpine:3.6

ENV PCRE_VER=8.41 \
	ZLIB_VER=1.2.11	\
	OPENSSL_VER=1.0.2k \
	NGINX_VER=1.13.4 \
	SRC=/usr \
	LIB=$SRC/local/include \
	BUILD_FOLDER=/nginx_build


RUN apk update && apk add wget alpine-sdk perl gcc git libxml2-dev libxslt-dev

WORKDIR ${BUILD_FOLDER}

RUN echo 'pcre' && \
	wget ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-${PCRE_VER}.tar.gz && \
	tar -zxf pcre-${PCRE_VER}.tar.gz && \
	cd pcre-${PCRE_VER} && \
	./configure && \
	make && \
	make install

RUN echo 'zlib' && \
	wget http://zlib.net/zlib-${ZLIB_VER}.tar.gz && \
	tar -zxf zlib-${ZLIB_VER}.tar.gz && \
	cd zlib-${ZLIB_VER} && \
	./configure && \
	make && \
	make install

RUN echo 'OpenSSL' && \
	wget http://www.openssl.org/source/openssl-${OPENSSL_VER}.tar.gz && \
	tar -zxf openssl-${OPENSSL_VER}.tar.gz && \
	cd openssl-${OPENSSL_VER} && \
	./Configure linux-x86_64 --prefix=/usr && \
	make && \
	make install

RUN echo 'NGINX-RTMP-Module' && \
	git clone https://github.com/arut/nginx-rtmp-module.git

# START FFMPEG

ENV FFMPEG_VERSION=3.0.2

WORKDIR /tmp/ffmpeg

RUN apk add --update build-base curl nasm tar bzip2 \
  zlib-dev yasm-dev lame-dev libogg-dev x264-dev libvpx-dev libvorbis-dev x265-dev freetype-dev libass-dev libwebp-dev rtmpdump-dev libtheora-dev opus-dev && \

  DIR=$(mktemp -d) && cd ${DIR} && \

  curl -s http://ffmpeg.org/releases/ffmpeg-${FFMPEG_VERSION}.tar.gz | tar zxvf - -C . && \
  cd ffmpeg-${FFMPEG_VERSION} && \
  ./configure \
  --enable-version3 --enable-gpl --enable-nonfree --enable-small --enable-libmp3lame --enable-libx264 --enable-libx265 --enable-libvpx --enable-libtheora --enable-libvorbis --enable-libopus --enable-libass --enable-libwebp --enable-librtmp --enable-postproc --enable-avresample --enable-libfreetype --enable-openssl --disable-debug && \
  make && \
  make install && \
  make distclean && \

  rm -rf ${DIR} && \
  apk del build-base curl tar bzip2 x264 nasm && rm -rf /var/cache/apk/*

WORKDIR ${BUILD_FOLDER}

RUN echo 'NGINX' && \
	wget http://nginx.org/download/nginx-${NGINX_VER}.tar.gz && \
	tar -zxf nginx-${NGINX_VER}.tar.gz && \
	cd nginx-${NGINX_VER} && \
	./configure \
	--sbin-path=/usr/sbin/nginx \
	--conf-path=/etc/nginx/conf/nginx.conf \
	--error-log-path=/var/log/nginx/error.log \
    --http-log-path=/var/log/nginx/access.log \
    --pid-path=/var/run/nginx.pid \
    --lock-path=/var/run/nginx.lock \
	--with-pcre=../pcre-${PCRE_VER} \
	--with-zlib=../zlib-${ZLIB_VER} \
	--with-http_ssl_module \
	--with-stream \
	--with-http_xslt_module \
	--add-module=../nginx-rtmp-module && \
	echo 'Making nginx' && \
	make && \
	make install && \
	cp ../nginx-rtmp-module/stat.xsl /usr/local/nginx/html/info.xsl && \
	wget "https://raw.githubusercontent.com/espizo/simple-nginx-rtmp/master/www/entities.dtd" && \
    mv entities.dtd /etc/nginx/conf/entities.dtd && \
    wget "https://raw.githubusercontent.com/espizo/simple-nginx-rtmp/master/www/xml2json.xsl" && \
    mv xml2json.xsl /etc/nginx/conf/xml2json.xsl

RUN echo 'Cache break'
COPY nginx.conf /etc/nginx/conf/nginx.conf
# Copy RTMP and HTTP into vhosts
COPY vhosts/ /etc/nginx/conf/vhosts/
COPY auth.htpasswd /etc/nginx/conf

RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log

RUN addgroup nginx \
    && adduser -H -D -G nginx nginx \
    && mkdir -p /www /var/log/nginx

#RUN ln -sf /dev/stdout /var/log/nginx/access.log
#RUN ln -sf /dev/stderr /var/log/nginx/error.log

CMD /usr/sbin/nginx